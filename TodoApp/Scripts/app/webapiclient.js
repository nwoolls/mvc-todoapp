﻿/*global $, ko, todo*/

//simple wrapper for Web API calls - passes in validation token if HTML is present

(function (ns) {
    "use strict";

    ns.webApiClient = (function () {

        var ajaxGet = function (method, input, callback, query) {

            var url = "/api/" + method;
            if (query) {
                url = url + "?" + query;
            }

            $.ajax({
                url: url,
                type: "GET",
                data: input,

                success: function (result) {
                    callback(result);
                }
            });
        };

        var ajaxPost = function (method, input, callback) {

            $.ajax({
                url: "/api/" + method + "/",
                type: "POST",
                data: input,

                success: function (result) {
                    callback(result);
                }
            });
        };

        var ajaxPut = function (method, id, input, callback) {

            $.ajax({
                url: "/api/" + method + "/" + id,
                type: "PUT",
                data: input,

                success: function (result) {
                    callback(result);
                }
            });
        };

        var ajaxDelete = function (method, id, callback) {

            $.ajax({
                url: "/api/" + method + "/" + id,
                type: "DELETE",

                success: function (result) {
                    callback(result);
                }
            });
        };

        return {
            ajaxGet: ajaxGet,
            ajaxPut: ajaxPut,
            ajaxPost: ajaxPost,
            ajaxDelete: ajaxDelete
        };
    }());

    //pass in namespace prefix (from namespace.js)
}(todo));