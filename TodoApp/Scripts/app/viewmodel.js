﻿/*global $, ko, todo*/

/* code for our ViewModel (no selectors in here) */

$((function (ns, webApiClient) {
    "use strict";

    ns.todoViewModel = (function () {

        //utilities
        function cloneJSModel(sourceModel, destinationModel) {
            destinationModel.Id(sourceModel.Id)
                .Text(sourceModel.Text)
                .Completed(sourceModel.Completed);
        }

        function cloneKOModel(sourceModel, destinationModel) {
            var jsModel = ko.toJS(sourceModel);
            cloneJSModel(jsModel, destinationModel);
        }

        //UI binding
        var items = ko.observableArray();

        //web api calls
        function populate() {

            webApiClient.ajaxGet("TodoItem", "", function (json) {
                items.removeAll();

                $.each(json, function (index, value) { //ignore jslint
                    var item = new ns.todoItemModel();
                    cloneJSModel(value, item);
                    items.push(item);
                });
            });
        }

        function addItem(todoItem) {

            webApiClient.ajaxPost("TodoItem", ko.toJS(todoItem), function (result) {
                var newItem = new ns.todoItemModel();
                cloneJSModel(result, newItem);
                items.push(newItem);
            });
        }

        function deleteItem(id) {

            webApiClient.ajaxDelete("TodoItem", id, function (result) {
                items.remove(function (item) {
                    return item.Id() === result.Id;
                });
            });
        }

        function updateItem(todoItem) {

            webApiClient.ajaxPut("TodoItem", todoItem.Id(), ko.toJS(todoItem), function () {
                var existingItem = ko.utils.arrayFirst(items(), function (item) {
                    return item.Id() === todoItem.Id();
                });
                cloneKOModel(todoItem, existingItem);
            });
        }

        //UI actions
        var addingItemText = ko.observable('');

        var canAddItem = ko.computed(function () {
            return addingItemText() !== "";
        });

        var addNewItem = function () {
            var newItem = new ns.todoItemModel();
            newItem.Text(addingItemText());
            addItem(newItem);
            addingItemText("");
        };

        var deleteSelectedItem = function () {
            deleteItem(this.Id());
        };

        var completeSelectedItem = function () {
            this.Completed(true);
            updateItem(this);
        };

        var undoSelectedItem = function () {
            this.Completed(false);
            updateItem(this);
        };

        //return a new object with the above items
        //bound as defaults for its properties
        return {
            items: items,
            populate: populate,
            addingItemText: addingItemText,
            canAddItem: canAddItem,
            addNewItem: addNewItem,
            deleteSelectedItem: deleteSelectedItem,
            completeSelectedItem: completeSelectedItem,
            undoSelectedItem: undoSelectedItem
        };

    }());

    ns.todoViewModel.populate();

    ko.applyBindings(ns.todoViewModel);

    //pass in namespace prefix (from namespace.js)
}(todo, todo.webApiClient)));