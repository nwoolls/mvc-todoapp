﻿/*global $, ko, todo*/

/* code for our Model (no selectors in here) */

$((function (ns) {
    "use strict";

    ns.todoItemModel = function () {
        var self = this;
        self.Id = ko.observable(0);
        self.Text = ko.observable('');
        self.Completed = ko.observable(false);
    };

    //pass in namespace prefix (from namespace.js)
}(todo)));