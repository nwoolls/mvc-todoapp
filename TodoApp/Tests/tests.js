﻿/// <reference path="../Scripts/jquery-1.9.1.js" />
/// 
/// <reference path="../Scripts/knockout-2.2.1.debug.js" />
/// 
/// <reference path="../Scripts/app/namespace.js" />
/// <reference path="webapiclient.stub.js" />
/// 
/// <reference path="../Scripts/app/model.js" />
/// <reference path="../Scripts/app/viewmodel.js" />

/*global test, todo, ok, module, equal*/

module("todo.viewmodel.populate");

test("todo.viewmodel.populate (0 length)", function () {
    "use strict";

    //arrange
    todo.webApiClient.testResult = [];

    //act
    todo.todoViewModel.populate();

    //assert
    equal(todo.todoViewModel.items().length, 0, "Passed!");
});

test("todo.viewmodel.populate (1 length)", function () {
    "use strict";

    //arrange
    todo.webApiClient.testResult = [
        {
            Id: 1,
            Text: "To-do",
            Completed: false
        }
    ];

    //act
    todo.todoViewModel.populate();

    //assert
    equal(todo.todoViewModel.items().length, 1, "Passed!");
});

module("todo.viewmodel.canAddNewItem");

test("todo.viewmodel.canAddNewItem (without text)", function () {
    "use strict";

    //arrange

    //act
    todo.todoViewModel.addingItemText('');

    //assert
    equal(todo.todoViewModel.canAddItem(), false, "Passed!");
});

test("todo.viewmodel.canAddNewItem (with text)", function () {
    "use strict";

    //arrange

    //act
    todo.todoViewModel.addingItemText('To-do');

    //assert
    equal(todo.todoViewModel.canAddItem(), true, "Passed!");
});

module("todo.viewmodel.addNewItem");

test("todo.viewmodel.addNewItem", function () {
    "use strict";

    //arrange
    todo.webApiClient.testResult = [];
    todo.todoViewModel.populate();

    var expectedItem = {
        Id: 1,
        Text: "To-do",
        Completed: false
    };

    todo.webApiClient.testResult = expectedItem;

    //act
    todo.todoViewModel.addingItemText(expectedItem.Text);
    todo.todoViewModel.addNewItem();

    //assert
    var firstItem = todo.todoViewModel.items()[0];
    equal(firstItem.Id(), expectedItem.Id, "Passed!");
});